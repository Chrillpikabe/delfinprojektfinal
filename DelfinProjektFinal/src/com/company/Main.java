package com.company;

import java.io.IOException;
import java.text.ParseException;

public class Main extends Control {

    public static void main(String[] args) throws ParseException, IOException  {

        Menu menu = new Menu();
        menu.læsBetalingsliste();
        menu.læsTotal(Totalliste);
        menu.læsTotal(Passivliste);
        menu.læsTotal(Motionist);
        menu.læsTotal(Konkurrence);
        menu.læsResultat();
        menu.Menu();

    }
}
