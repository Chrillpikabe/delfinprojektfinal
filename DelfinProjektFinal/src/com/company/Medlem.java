package com.company;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Medlem{

    private int medlemsnummer;
    private String fornavn;
    private String efternavn;
    private Date fødselsdag;
    private int oprettelsesår;

    public void setOprettelseår(int oprettelseår) {
        this.oprettelsesår = oprettelseår;
    }

    public int getOprettelseår() {
        return oprettelsesår;
    }
    private String tlf;
    private String email;
    private int medlemstype;
    DateFormat d = new SimpleDateFormat("dd/MM/yyyy");

    public String getEfternavn() {
        return efternavn;
    }
    public void setMedlemsnummer(int medlemsnummer) {
        this.medlemsnummer = medlemsnummer;
    }

    public void setFornavn(String fornavn) {
        this.fornavn = fornavn;
    }
    public void setFødselsdag(Date fødselsdag) {
        this.fødselsdag = fødselsdag;
    }


    public void setTlf(String tlf) {
        this.tlf = tlf;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMedlemstype(int medlemstype) {
        this.medlemstype = medlemstype;
    }

    public int getMedlemsnummer() {
        return medlemsnummer;
    }

    public String getFornavn() {
        return fornavn;
    }

    public Date getfødselsdag() {
        return fødselsdag;
    }

    public String getTlf() {
        return tlf;
    }

    public String getEmail() {
        return email;
    }

    public int getMedlemstype() {
        return medlemstype;
    }

    public Medlem(int medlemsnummer, String fornavn, int oprettelseår) {
        this.medlemsnummer = medlemsnummer;
        this.fornavn = fornavn;
        this.oprettelsesår = oprettelseår;
    }


    public Medlem(int medlemsnummer, String fornavn, String efternavn, String fødselsdato, String tlf, String email, int medlemstype, int opretellseår) throws ParseException {
        Date fødselsdag = d.parse(fødselsdato);
        this.medlemsnummer = medlemsnummer;
        this.fornavn = fornavn;
        this.efternavn = efternavn;
        this.fødselsdag = fødselsdag;
        this.tlf = tlf;
        this.email = email;
        this.medlemstype = medlemstype;
        this.oprettelsesår = opretellseår;
    }




}
