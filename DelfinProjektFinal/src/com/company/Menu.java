package com.company;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

public class Menu extends Control {

    public void Menu() throws ParseException, IOException {
        System.out.println("Velkommen Til delfinprogrammet");
        Scanner scanner = new Scanner(System.in);
        String svar;
        System.out.println("-------------------------------------------------------");
        boolean t = true;
        while (t) {
            System.out.println("");
            System.out.println("tast 1 for oprette et medlem");
            System.out.println("tast 2 for at se total medlemsliste");
            System.out.println("tast 3 for at se passiv medlemsliste");
            System.out.println("tast 4 for at se motionist medlemsliste");
            System.out.println("tast 5 for at se konkurrence medlemsliste");
            System.out.println("tast 6 for at indtaste Resultat");
            System.out.println("tast 7 for at se Top 5 liste");
            System.out.println("Tast 8 for at se restanceliste");
            System.out.println("Tast 9 for at fjerne medlem fra restanceliste");
            Scanner scanner1 = new Scanner(System.in);
            int svar1;
            svar1 = scanner.nextInt();
            switch (svar1) {
                case 1:
                    Control medlem = new Control();
                    medlem.Opretmedlem();
                    break;
                case 2:
                    printotal();
                    break;
                case 3:
                    printpassiv();
                    break;
                case 4:
                    printmotion();
                    break;
                case 5:
                    printkon();
                    break;
                case 6:
                    Opretresultat();
                    break;
                case 7:
                    printTop5();
                    break;
                case 8:
                    Opkrævning();
                    printbetalingsliste();
                    break;
                case 9:
                    System.out.println("Angiv medlemsnr på person der skal fjernes");
                    svar1 = scanner.nextInt();
                    betal(svar1);
                    break;
                default:
                    System.out.println("Findes ikke på menuen prøv igen");
                    break;
            }

        }
    }

}