
package com.company;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Injam DCF
 */
public class konkurrencesvømmer extends Control {

    public Date getStævne() {
        return stævne;
    }

    public String getResultat() {
        return resultat;
    }

    public int getPlacering() {
        return placering;
    }

    public String getDisciplin() {
        return Disciplin;
    }

    public String getFornavn() {
        return fornavn;
    }

    public int getNr() {
        return nr;
    }

    public DateFormat getD() {
        return d;
    }
    private Date stævne;
    private String resultat;
    private int placering;
    private String Disciplin;
    private String fornavn;
    private int nr;
    DateFormat d = new SimpleDateFormat("dd/MM/yyyy");

    public konkurrencesvømmer(int nr, String fornavn, String stævne, String resultat, int placering, String Disciplin) throws ParseException {

        Date stævner = d.parse(stævne);
        this.fornavn = fornavn;
        this.nr = nr;
        this.stævne = stævner;
        this.resultat = resultat;
        this.placering = placering;
        this.Disciplin = Disciplin;

    }
}