package com.company;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Scanner;

public class Control {

    DateFormat d = new SimpleDateFormat("dd/MM/yyyy");
    ArrayList<Medlem> totalmedlemsliste = new ArrayList<>();
    ArrayList<Medlem> passivmedlemsliste = new ArrayList<>();
    ArrayList<Medlem> motionistmedlemsliste = new ArrayList<>();
    ArrayList<Medlem> betalingsliste = new ArrayList<>();
    ArrayList<Medlem> konkurrencemedlemsliste = new ArrayList<>();
    ArrayList<konkurrencesvømmer> resultatliste = new ArrayList<>();
    static final String Totalliste = "Total.txt";
    static final String Passivliste = "Passiv.txt";
    static final String Konkurrence = "Konkurrence.txt";
    static final String Motionist = "Motionst.txt";
    static final String Resultat1 = "Resultat.txt";
    static final String Betallingsliste = "Betallingsliste.txt";

    public void Opretmedlem() throws ParseException {

        int medlemsnummer;
        String fornavn;
        String efternavn;
        String fødselsdag;
        String tlf;
        String email;
        int oprettelseår;
        int medlemstype;
        Scanner scanner = new Scanner(System.in);
        int svar;
        boolean t = true;
        String svar1;
        while (t == true) {

            System.out.println("indtast medlemsnummer");
            svar = scanner.nextInt();
            medlemsnummer = svar;

            System.out.println("indtast fornavn");
            svar1 = scanner.next();
            fornavn = svar1;

            System.out.println("indtast efternavn");
            svar1 = scanner.next();
            efternavn = svar1;

            System.out.println("indtast fødselsdato");
            svar1 = scanner.next();
            fødselsdag = svar1;

            System.out.println("indtast tlfnr");
            svar1 = scanner.next();
            tlf = svar1;

            System.out.println("indtast email");
            svar1 = scanner.next();
            email = svar1;

            System.out.println("indtast medlemstype: 1 for passivt, 2 for motionist, 3 for konkurrencesvømmer");
            svar = scanner.nextInt();
            medlemstype = svar;

            System.out.println("indtast oprettelsesår");
            svar = scanner.nextInt();
            oprettelseår = svar;

            totalmedlemsliste.add(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår));

            skrivTilTotal(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår));

            switch (medlemstype) {
                case 1:
                    passivmedlemsliste.add(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår));
                    skrivTilFil(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår), Passivliste);
                    break;
                case 2:
                    motionistmedlemsliste.add(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår));
                    skrivTilFil(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår), Motionist);
                    break;
                case 3:
                    skrivTilFil(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår), Konkurrence);
                    konkurrencemedlemsliste.add(new Medlem(medlemsnummer, fornavn, efternavn, fødselsdag, tlf, email, medlemstype, oprettelseår));
                    break;
                default:
                    break;
            }

            System.out.println("vil du tilføje flere medlemmer?");
            svar1 = scanner.next();
            if (svar1.equalsIgnoreCase("nej")) {
                t = false;
            }
        }

    }

    public void Opretresultat() throws ParseException {
        int medlemsnummer;
        String fornavn;
        String Stævne;
        String resultat;
        int placering;
        String Disciplin;
        Scanner scanner = new Scanner(System.in);
        int svar;
        double svar3;
        boolean t = true;
        String svar1;
        while (t == true) {

            System.out.println("indtast medlemsnummer");
            svar = scanner.nextInt();
            medlemsnummer = svar;

            System.out.println("indtast fornavn");
            svar1 = scanner.next();
            fornavn = svar1;

            System.out.println("indtast Dato");
            svar1 = scanner.next();
            Stævne = svar1;

            System.out.println("indtast Resultat");
            svar1 = scanner.next();
            resultat = svar1;

            System.out.println("indtast Placering");
            svar = scanner.nextInt();
            placering = svar;

            System.out.println("indtast Disciplin");
            svar1 = scanner.next();
            Disciplin = svar1;

            resultatliste.add(new konkurrencesvømmer(medlemsnummer, fornavn, Stævne, resultat, placering, Disciplin));
            skrivResultat(new konkurrencesvømmer(medlemsnummer, fornavn, Stævne, resultat, placering, Disciplin));

            System.out.println("vil du tilføje flere Resultater");
            svar1 = scanner.next();
            if (svar1.equalsIgnoreCase("nej")) {
                t = false;
            }

        }

    }

    public void læsTotal(String s) throws ParseException {

        try (BufferedReader br = new BufferedReader(new FileReader(s))) {
            String line = null;

            while ((line = br.readLine()) != null) {

                String[] parts = line.split(",");
                int nummer = Integer.parseInt(parts[0]);
                String Fornavn = parts[1];
                String Efternavn = parts[2];
                String alder = parts[3];
                String tlf = parts[4];
                String Email = parts[5];
                String opretellsesår = parts[6];
                int type = 0;
                if(s.equalsIgnoreCase(Totalliste)){
                totalmedlemsliste.add(new Medlem(nummer, Fornavn, Efternavn, alder, tlf, Email, type, Integer.parseInt(opretellsesår)));
                }
                if(s.equalsIgnoreCase(Passivliste)){
                    passivmedlemsliste.add(new Medlem(nummer, Fornavn, Efternavn, alder, tlf, Email, type, Integer.parseInt(opretellsesår)));
                }
                if(s.equalsIgnoreCase(Motionist)){
                    motionistmedlemsliste.add(new Medlem(nummer, Fornavn, Efternavn, alder, tlf, Email, type, Integer.parseInt(opretellsesår)));
                }
                if(s.equalsIgnoreCase(Konkurrence)){
                    konkurrencemedlemsliste.add(new Medlem(nummer, Fornavn, Efternavn, alder, tlf, Email, type, Integer.parseInt(opretellsesår)));
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void skrivTilFil(Medlem medlem, String s) {

        try (
                FileWriter fw = new FileWriter(s, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {

            out.println(medlem.getMedlemsnummer() + "," + medlem.getFornavn() + ","
                    + medlem.getEfternavn() + ","
                    + d.format(medlem.getfødselsdag()) + "," + medlem.getTlf() + "," + medlem.getEmail()+","+medlem.getOprettelseår());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void skrivResultat(konkurrencesvømmer k) {

        try (
                FileWriter fw = new FileWriter(Resultat1, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {

            out.println(k.getNr() + "," + k.getFornavn() + ","
                    + d.format(k.getStævne()) + "," + k.getResultat() + "," + k.getPlacering() + "," + k.getDisciplin());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void læsResultat() throws ParseException {

        try (BufferedReader br = new BufferedReader(new FileReader(Resultat1))) {
            String line = null;

            while ((line = br.readLine()) != null) {

                String[] parts = line.split(",");
                int nummer = Integer.parseInt(parts[0]);
                String Fornavn = parts[1];
                String Dates = parts[2];
                String tid = parts[3];
                int plads = Integer.parseInt(parts[4]);
                String Disciplin = parts[5];
                resultatliste.add(new konkurrencesvømmer(nummer, Fornavn, Dates, tid, plads, Disciplin));
                //totalmedlemsliste.add(new Medlem(nummer, Fornavn, Efternavn, alder, tlf, Email, type));
                // System.out.println("Medlemsnummer: " + nummer + " Navn: " + Fornavn + " " + Efternavn + " Fødelsdato: " + alder + " Tlf: " + tlf + " Email: " + Email)
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void printTop5() throws IOException, ParseException {
        // Collections er java metode. Sort sortere uanset string eller int.
        Collections.sort(resultatliste, new sammenligner());
        Scanner s = new Scanner(System.in);
        int svar;
        System.out.println("Tast 1 for Crawl");
        System.out.println("Tast 2 for Hundesvømning");
        System.out.println("Tast 3 for Brystsvømning");
        System.out.println("Tast 4 for Rygcrawl");
        System.out.println("Tast 5 for Butterfly");
        svar = s.nextInt();
        ArrayList<konkurrencesvømmer> CrawlTop5 = new ArrayList<>();
        ArrayList<konkurrencesvømmer> ButterflyTop5 = new ArrayList<>();
        ArrayList<konkurrencesvømmer> BrystTop5 = new ArrayList<>();
        ArrayList<konkurrencesvømmer> HundTop5 = new ArrayList<>();
        ArrayList<konkurrencesvømmer> RygcrawlTop5 = new ArrayList<>();
        for (konkurrencesvømmer m : resultatliste) {

            if (m.getDisciplin().equalsIgnoreCase("Crawl")) {
                CrawlTop5.add(m);
            }

            if (m.getDisciplin().equalsIgnoreCase("Butterfly")) {
                ButterflyTop5.add(m);
            }
            if (m.getDisciplin().equalsIgnoreCase("Bryst")) {
                BrystTop5.add(m);
            }
            if (m.getDisciplin().equalsIgnoreCase("Hunde")) {
                HundTop5.add(m);
            }
            if (m.getDisciplin().equalsIgnoreCase("Rygcrawl")) {
                RygcrawlTop5.add(m);
            }
        }
        if (svar == 1) {
            for (int i = 0; i <= 4; i++) {
                System.out.println("Medlemsnummer: " + CrawlTop5.get(i).getNr() + " Navn: " + CrawlTop5.get(i).getFornavn() + " Stævne Dato: " + d.format(CrawlTop5.get(i).getStævne()) + " " + "Tid: " + CrawlTop5.get(i).getResultat() + " Placering: " + CrawlTop5.get(i).getPlacering() + " Disciplin: " + CrawlTop5.get(i).getDisciplin());

            }
        }
        if (svar == 2) {
            for (int i = 0; i <= 4; i++) {
                System.out.println("Medlemsnummer: " + HundTop5.get(i).getNr() + " Navn: " + HundTop5.get(i).getFornavn() + " Stævne Dato: " + d.format(HundTop5.get(i).getStævne()) + " " + "Tid: " + HundTop5.get(i).getResultat() + " Placering: " + HundTop5.get(i).getPlacering() + " Disciplin: " + HundTop5.get(i).getDisciplin());

            }
        }
        if (svar == 3) {
            for (int i = 0; i <= 4; i++) {
                System.out.println("Medlemsnummer: " + BrystTop5.get(i).getNr() + " Navn: " + BrystTop5.get(i).getFornavn() + " Stævne Dato: " + d.format(BrystTop5.get(i).getStævne()) + " " + "Tid: " + BrystTop5.get(i).getResultat() + " Placering: " + BrystTop5.get(i).getPlacering() + " Disciplin: " + BrystTop5.get(i).getDisciplin());

            }
        }
        if (svar == 4) {
            for (int i = 0; i <= 4; i++) {
                System.out.println("Medlemsnummer: " + RygcrawlTop5.get(i).getNr() + " Navn: " + RygcrawlTop5.get(i).getFornavn() + " Stævne Dato: " + d.format(RygcrawlTop5.get(i).getStævne()) + " " + "Tid: " + RygcrawlTop5.get(i).getResultat() + " Placering: " + RygcrawlTop5.get(i).getPlacering() + " Disciplin: " + RygcrawlTop5.get(i).getDisciplin());

            }
        }
        if (svar == 5) {
            for (int i = 0; i <= 4; i++) {
                System.out.println("Medlemsnummer: " + ButterflyTop5.get(i).getNr() + " Navn: " + ButterflyTop5.get(i).getFornavn() + " Stævne Dato: " + d.format(ButterflyTop5.get(i).getStævne()) + " " + "Tid: " + ButterflyTop5.get(i).getResultat() + " Placering: " + ButterflyTop5.get(i).getPlacering() + " Disciplin: " + ButterflyTop5.get(i).getDisciplin());
            }
        }

    }

    public static class sammenligner implements Comparator<konkurrencesvømmer> {

        @Override
        public int compare(konkurrencesvømmer o1, konkurrencesvømmer o2) {
            // Sammenligner alle pickuptimes og sætte dem i orden gennem collections.sort.
            return o1.getResultat().compareTo(o2.getResultat());
        }

    }

    public void betal(int medlemsnr) {
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        for (int i = 0; i < betalingsliste.size(); i++) {

            if (betalingsliste.get(i).getMedlemsnummer() == medlemsnr) {
                for (int j = 0; j < totalmedlemsliste.size(); j++) {

                    if (totalmedlemsliste.get(j).getMedlemsnummer() == medlemsnr) {
                        totalmedlemsliste.get(j).setOprettelseår(year);
                    }
                    skrivTilTotal(totalmedlemsliste.get(j));
                }
                betalingsliste.remove(i);

                if (betalingsliste.size() != 0) {
                    skrivTilBetaling(betalingsliste.get(i));

                } else {
                    skrivTilBetaling(null);
                }

            }

        }
    }

    public void Opkrævning() {
        for (int i = 0; i < totalmedlemsliste.size(); i++) {
            int test = 2;
            int medlemsnummer = totalmedlemsliste.get(i).getMedlemsnummer();
            String navn = totalmedlemsliste.get(i).getFornavn();
            int oprettelsesår = totalmedlemsliste.get(i).getOprettelseår();

            Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);

            totalmedlemsliste.get(i);

            if (year > totalmedlemsliste.get(i).getOprettelseår()) {
                for (int j = 0; j < betalingsliste.size(); j++) {

                    if (betalingsliste.size() == 0) {
                        betalingsliste.add(new Medlem(medlemsnummer, navn, oprettelsesår));
                        skrivTilBetaling(new Medlem(medlemsnummer, navn, oprettelsesår));
                    } else if (betalingsliste.get(j).getMedlemsnummer() == medlemsnummer) {
                        test = 1;
                        break;
                    } else {
                        test = 2;
                    }

                }
                if (test == 2) {
                    betalingsliste.add(new Medlem(medlemsnummer, navn, oprettelsesår));
                    skrivTilBetaling(new Medlem(medlemsnummer, navn, oprettelsesår));

                }

            }

        }

    }

    public void skrivTilTotal(Medlem medlem) {

        try (
                FileWriter fw = new FileWriter(Totalliste, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            for (int i = 0; i < totalmedlemsliste.size(); i++) {
                out.println(totalmedlemsliste.get(i).getMedlemsnummer() + "," + totalmedlemsliste.get(i).getFornavn() + ","
                        + totalmedlemsliste.get(i).getEfternavn() + "," + d.format(medlem.getfødselsdag()) + ","
                        + totalmedlemsliste.get(i).getTlf() + "," + totalmedlemsliste.get(i).getEmail() + ","
                        + totalmedlemsliste.get(i).getOprettelseår());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void læsBetalingsliste() throws ParseException {

        try (BufferedReader br = new BufferedReader(new FileReader(Betallingsliste))) {
            String line = null;

            while ((line = br.readLine()) != null) {

                String[] parts = line.split(",");
                int nummer = Integer.parseInt(parts[0]);
                String Fornavn = parts[1];
                String oprettelsesår = parts[2];
                betalingsliste.add(new Medlem(nummer, Fornavn, Integer.parseInt(oprettelsesår)));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void skrivTilBetaling(Medlem medlem) {

        try (
                FileWriter fw = new FileWriter(Betallingsliste, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            if (medlem == null) {
                out.flush();
            } else {
                for (int i = 0; i < betalingsliste.size(); i++) {

                    out.println(betalingsliste.get(i).getMedlemsnummer() + "," + betalingsliste.get(i).getFornavn() + "," + betalingsliste.get(i).getOprettelseår());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void printbetalingsliste() {
        for (int i = 0; i < betalingsliste.size(); i++) {
            int nummmer = betalingsliste.get(i).getMedlemsnummer();
            String fornavn = betalingsliste.get(i).getFornavn();
            int oprettelsår = betalingsliste.get(i).getOprettelseår();
            System.out.println("Medlemsnummer" + nummmer + " Navn:" + fornavn + " opretelsesår: " + oprettelsår);

        }
    }

    public void printpassiv() {
        if (passivmedlemsliste.isEmpty()) {
            System.out.println("Listen er tom");
        } else {
            for (int i = 0; i < passivmedlemsliste.size(); i++) {
                int nummmer = passivmedlemsliste.get(i).getMedlemsnummer();
                String fornavn = passivmedlemsliste.get(i).getFornavn();
                String Efternavn = passivmedlemsliste.get(i).getEfternavn();
                Date alder = passivmedlemsliste.get(i).getfødselsdag();
                String tlf = passivmedlemsliste.get(i).getTlf();
                String email = passivmedlemsliste.get(i).getEmail();
                int oprettelsår = passivmedlemsliste.get(i).getOprettelseår();
                System.out.println("Medlemsnummer: " + nummmer + " Navn: " + fornavn + " " + Efternavn + " Fødelsdato: " + d.format(alder) + " Tlf: " + tlf + " Email: " + email);
            }
        }
    }

    public void printmotion() {
        if (motionistmedlemsliste.isEmpty()) {
            System.out.println("Listen er tom");
        } else {
            for (int i = 0; i < motionistmedlemsliste.size(); i++) {
                int nummmer = motionistmedlemsliste.get(i).getMedlemsnummer();
                String fornavn = motionistmedlemsliste.get(i).getFornavn();
                String Efternavn = motionistmedlemsliste.get(i).getEfternavn();
                Date alder = motionistmedlemsliste.get(i).getfødselsdag();
                String tlf = motionistmedlemsliste.get(i).getTlf();
                String email = motionistmedlemsliste.get(i).getEmail();
                int oprettelsår = motionistmedlemsliste.get(i).getOprettelseår();
                System.out.println("Medlemsnummer: " + nummmer + " Navn: " + fornavn + " " + Efternavn + " Fødelsdato: " + d.format(alder) + " Tlf: " + tlf + " Email: " + email);
            }
        }
    }

    public void printkon() {
        if (konkurrencemedlemsliste.isEmpty()) {
            System.out.println("Listen er tom");
        } else {
            for (int i = 0; i < konkurrencemedlemsliste.size(); i++) {
                int nummmer = konkurrencemedlemsliste.get(i).getMedlemsnummer();
                String fornavn = konkurrencemedlemsliste.get(i).getFornavn();
                String Efternavn = konkurrencemedlemsliste.get(i).getEfternavn();
                Date alder = konkurrencemedlemsliste.get(i).getfødselsdag();
                String tlf = konkurrencemedlemsliste.get(i).getTlf();
                String email = konkurrencemedlemsliste.get(i).getEmail();
                int oprettelsår = konkurrencemedlemsliste.get(i).getOprettelseår();
                System.out.println("Medlemsnummer: " + nummmer + " Navn: " + fornavn + " " + Efternavn + " Fødelsdato: " + d.format(alder) + " Tlf: " + tlf + " Email: " + email);
            }
        }
    }
    public void printotal() {
        if (totalmedlemsliste.isEmpty()) {
            System.out.println("Listen er tom");
        } else {
            for (int i = 0; i < totalmedlemsliste.size(); i++) {
                int nummmer = totalmedlemsliste.get(i).getMedlemsnummer();
                String fornavn = totalmedlemsliste.get(i).getFornavn();
                String Efternavn =totalmedlemsliste.get(i).getEfternavn();
                Date alder = totalmedlemsliste.get(i).getfødselsdag();
                String tlf = totalmedlemsliste.get(i).getTlf();
                String email = totalmedlemsliste.get(i).getEmail();
                int oprettelsår = totalmedlemsliste.get(i).getOprettelseår();
                System.out.println("Medlemsnummer: " + nummmer + " Navn: " + fornavn + " " + Efternavn + " Fødelsdato: " + d.format(alder) + " Tlf: " + tlf + " Email: " + email);
            }
        }
    }
}